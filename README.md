# ct (control toolbox) gallery

Jupyter notebooks for use cases using solvers of `bocop` & `nutopy` packages and others.

[![Online examples](https://img.shields.io/badge/ct-gallery-blue.svg)](https://ct.gitlabpages.inria.fr/gallery)
[![Full directory](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fct%2Fgallery.git/master?urlpath=lab/)

Special thanks: [A. Hurtig](http://www.alain.les-hurtig.org) for the logos ligatures

## Procedure to get the examples and install the packages

In the following, `ROOT` is a variable standing for the root directory where the course repository will be stored. For example, 
```bash
export ROOT=${HOME}
```

The examples are given in the form of python notebooks. To download all the examples, use the `git` command ([git](https://git-scm.com/downloads)). To clone the git repository and get the examples:
```bash
cd $ROOT
git clone https://gitlab.inria.fr/ct/gallery.git
```

The examples of our gallery uses python packages (see `pkg/env` directory). All theses packages may be installed in a conda environment for ease of use.
If you do not have the `conda` command, please install [miniconda](https://docs.conda.io/en/latest/miniconda.html) or [conda](https://docs.conda.io/en/latest/) (conda is longer to install than miniconda). Then, create the conda environment:
```bash
cd $ROOT/gallery
conda env create -f pkg/env/gallery-<YOUR_OS>.yaml
```
Where <YOUR_OS> must be replaced by ```linux```, ```mac``` or ```windows```, according to the platform used.

> If you only want to install the packages, that is you do not want to download all the examples, you can simply 
> download the file `pkg/env/gallery-<YOUR_OS>.yaml` and then create the conda environment with:
> ```bash
> conda env create -f gallery-<YOUR_OS>.yaml
> ```

Activate the gallery conda environment to get access to all the packages:
```bash
conda activate gallery-dev
```

Run jupyterlab and access to the examples (in subdirectory `examples`):
```bash
jupyter-lab
```

## Revoming the environment

You can remove the gallery environment doing:
```bash
conda activate base
conda env remove --name gallery-dev
```
