using Zygote, OrdinaryDiffEq

function Flow(h)

    function hv(t, x, p, args)
        foo = (x, p) -> h(t, x, p, args)
        dhdx, dhdp = Zygote.gradient(foo, x, p)
        return [ dhdp ; -dhdx ]
    end

    function rhs!(dz, z, args, t)
        n = size(z, 1)÷2
        x, p = z[1:n], z[n+1:2*n]
        dz[:] = hv(t, x, p, args)
    end
    
    function f(t0, x0, p0, tf, args)
        n = size(x0, 1)
        z0 = [ x0 ; p0 ]
        ode = ODEProblem(rhs!, z0, (t0, tf), args)
        z = solve(ode, Tsit5(), abstol=1e-12, reltol=1e-12) # todo: options with defaults (see args...)
        return z[end][1:n], z[end][n+1:2*n]
    end
    
    return f
    
end
