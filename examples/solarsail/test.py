import numpy as np
#from   gve   import kepler, gve, gveeci
from   hfun  import *

#############################
# TEST
#############################
mu      = 1.
I       = np.array([0.1, 0.2, 0.3, 1., 0.4])
M       = 0.5

alpha   = 60. * np.pi / 180.
fCone   = np.array([np.cos(alpha), np.sin(alpha)])

b       = np.array([0.1728, 1.6544, - 0.0109 * 0.])

pars    = np.hstack((mu, I, fCone, b))

q       = np.array([0., 0., 0., 0., 0.])
p       = np.array([1., 0., -1., 0., 0.])

H       = hfun_u(M, q, p, pars, 0.9)

print(H)
