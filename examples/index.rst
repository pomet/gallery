Welcome to ct gallery
#####################

Examples from the ct (control toolbox) project
using `bocop <https://ct.gitlabpages.inria.fr/bocop3>`_, `nutopy <https://ct.gitlabpages.inria.fr/nutopy>`_ and other packages.

Dashboards
**********

.. image:: bacteria/bacteria-panel.jpeg
    :height: 100
    :target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fct%2Fgallery-env.git/master?urlpath=%2Fproxy%2F5006%2Fbacteria-panel

.. image:: swimmer/swimmer.jpeg
    :height: 100
    :target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fct%2Fgallery-env.git/master?urlpath=%2Fproxy%2F5007%2Fswimmer

Notebooks
*********

.. nbgallery::
    :caption: Notebooks
    :name: ocp-gallery
    :glob:
    :reversed:

    sir/SIR
    nlp_indirect/nlp2
    nlp_indirect/nlp1
    ihm/ihm
    geometry2D/ellipsoid/ellipsoid
    substrate/depletion
    bacteria/bacteria
    bistable/bistable
    clr/clr
    nav/nav-julia-N3
    nav/nav
    solarsail/solarsail-simple-version
    kepler-c/kepler-c
    kepler-py/kepler-py
    kepler/kepler
    regulator/regulator
    goddard/goddard
    smooth_case/smooth_case
    shooting_tutorials/simple_shooting_general
