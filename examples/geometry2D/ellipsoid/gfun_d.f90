!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.8 (r4996) - 25 Oct 2013 15:08
!
!  Differentiation of gfun in forward (tangent) mode (with options fixinterface):
!   variations   of useful results: g
!   with respect to varying inputs: x
!   RW status of diff variables: g:out x:in
SUBROUTINE GFUN_D(x, xd, a, b, c, g, gd)
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(IN) :: x(2), a, b, c
  DOUBLE PRECISION, INTENT(IN) :: xd(2)
  DOUBLE PRECISION, INTENT(OUT) :: g(2)
  DOUBLE PRECISION, INTENT(OUT) :: gd(2)
  DOUBLE PRECISION :: l1, l2, th1, th2
  DOUBLE PRECISION :: l1d, l2d, th1d, th2d
  INTRINSIC SIN
  INTRINSIC COS
  th1d = xd(1)
  th1 = x(1)
  th2d = xd(2)
  th2 = x(2)
  l1d = a*2*SIN(th1)*th1d*COS(th1) - b*2*COS(th1)*th1d*SIN(th1)
  l1 = a*SIN(th1)**2 + b*COS(th1)**2
  l2d = c*2*SIN(th2)*th2d*COS(th2) - b*2*COS(th2)*th2d*SIN(th2)
  l2 = b*COS(th2)**2 + c*SIN(th2)**2
  gd = 0.D0
  gd(1) = (((l1d-l2d)*l1+(l1-l2)*l1d)*(l1-c)-(l1-l2)*l1*l1d)/(l1-c)**2
  g(1) = (l1-l2)*l1/(l1-c)
  gd(2) = (((l1d-l2d)*l2+(l1-l2)*l2d)*(a-l2)+(l1-l2)*l2*l2d)/(a-l2)**2
  g(2) = (l1-l2)*l2/(a-l2)
END SUBROUTINE GFUN_D

