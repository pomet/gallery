subroutine gfun(x, a, b, c, g)

    double precision, intent(in)  :: x(2), a, b, c
    double precision, intent(out) :: g(2)

    double precision :: l1, l2, th1, th2

    th1 = x(1)
    th2 = x(2)

    l1 = a * sin(th1)**2 + b * cos(th1)**2
    l2 = b * cos(th2)**2 + c * sin(th2)**2

    g(1) = (l1-l2)*l1/(l1-c)
    g(2) = (l1-l2)*l2/(a-l2)

end subroutine gfun
