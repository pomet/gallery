subroutine hfun(x, p, h, gfun)

    double precision, intent(in)  :: x(2), p(2)
    double precision, intent(out) :: h

    interface
        subroutine gfun(x, g)
        double precision, intent(in)  :: x(2)
        double precision, intent(out) :: g(2)
        end subroutine gfun
    end interface

    ! local variables
    double precision :: g(2), p1, p2

    p1  = p(1)
    p2  = p(2)

    call gfun(x, g)

    h = 0.5d0 * (p1**2 / g(1) + p2**2 / g(2))

end subroutine hfun
